![roboarm_diy_version](img/robossembler-arm.png)

# Робот-манипулятор Robossembler

> ВНИМАНИЕ! В настоящее время манипулятор находится в фазе активной разработки и прототипирования, поэтому опубликованные исходные файлы пока не стоит использовать для изготовления прототипа. По мере готовности все необходимые файлы для производства будут опубликованы в *Реестре Пакетов (Gitlab Package Registry)* или *Релизах (Releases)* данного репозитория

Доступный открытый промышленный робот-манипулятор с 6-ю степенями свободы, адаптированный для автоматической сборки. Все детали, кроме электрических плат и компонентов, магнитопровода, постоянных магнитов и катушек индуктивности, могут быть изготовлены с помощью технологии трёхмерной печати.

## Ключевые особенности

* **Open Source**
	* Все разработанные 3D-модели, исходные коды программ, электрические схемы открыты и доступны для копирования и модификации
	* Все файлы созданы с помощью свободных программ (KiCAD, Blender) или публикуются в открытых форматах (STEP)
* **Векторное управление серводвигателями**
	* Энергоэффективность
	* Простота охлаждения
* **Универсальный симметричный стыковочный механизм**
	* Гибкость, модульность
	* Возможность перемещения робота в другие посадочные места
* **Управление звеньям робота на уровне Realtime-протокола DDS**
	* ROS2 совместимость по умолчанию на уровне звеньев
	* Полное отсутствие проприетарных протоколов
* **Сборочно-ориентированный дизайн с печатными деталями без единого болта**
	* Быстрая сборка робота вручную без использования специальных инструментов
	* Возможность полной автоматизации сборки
* **Интеграция с сетью Robonomics**
	* Безопасность в удалённом управлении роботом и обновлении ПО
	* Инструменты эффективного федеративного обучения роботов, формирования базы мета-навыков и предсказаний сбоев

## Компоненты

| Опорное звено | "Вилка" | Основное звено | Конечное звено |
|------|-------|------|-------|
| ![](img/base-link.jpg) | ![](img/fork.jpg) | ![](img/link.jpg) | ![](img/end-effector-link.jpg) |

## Основные характеристики

| Характеристика | Значение |
|------|-------|
| Число степеней свободы (Degrees of Freedom) | 6 DoF |
| Диапазоны движения звеньев (Motion Range) | A – 360”, B – 270”, C – 360”, D – 270”, E – 360”, F – 270” |
| Максимальная скорость (Max speed), град/с | 30 |
| Повторяемость (Pose Repeatability), мм | 0.1 |
| Момент силы (Rated torque), Н*м | 78 |
| Максимальная нагрузка (Max payload), кг | 2 |
| Максимальная потребляемая мощность (Max Consumption Power), Вт | 150 |
| Производимый шум (Acoustic noise level), dB | 65 |
| Влагозащищенность (water-proof) | IP65 |
| Вес (Weight), кг | 4,79 (конечное звено 0,63, соединительная вилка 1,68, соединительное звено 1,58, опорное звено 0,9) |
| Высота (Height), мм | 900 |
| Дальность (Reach), мм | 600 |
| Размер места для монтажа (Footprint), мм | 200х200 |
| Масштабирование/модульность | может быть собран в конфигурациях с кратным двум количеством степеней свободы - 2,4,6,8 |
| Robotics framework | ROS2 |
| Интерфейсы (Communication Interfaces) | RS485 |
| Безопасность (Security) | Robonomics Network & ROS2 Security  |
| Автоматическое обновление ПО (Automatic updates) | Robonomics Network |

## Приспособления

Приспособления монтируются к конечному звену манипулятора через стыковочный интерфейс. На данный момент наиболее проработан [Механический захват](https://gitlab.com/robosphere/arm-tools/grip-tool).

## Обзор директорий и файлов

* **brd** - исходные файлы печатных плат в формате [KiCAD](https://www.kicad.org/)
* **calc** - расчёты механических узлов манипулятора
* **img** - изображения моделей, включая предыдущие версии
* **src** - исходные файлы деталей конструкции в формате Solidworks. Со временем будут переведены во FreeCAD. Сейчас же доступна STEP-сборка (arm_assembly) в корне репозитория
* **BOM_price_spec.xls** - закупочная ведомость
* **assembly.xls** - карта сборки

## Степень готовности

* Механическая конструкция разработана и в настоящий момент прототипируется
* Печатные платы (контроллер мотора, датчик угла поворота) разработаны и заказаны
* Встроенное программное обеспечение в разработке. Мы планируем использовать и дорабатывать открытые библиотеки [SimpleFOC](https://github.com/simplefoc/Arduino-FOC), [microROS](https://micro.ros.org/)
* Программное обеспечение ROS2 в разработке - см. репозиторий [robossembler-ros2](https://gitlab.com/robosphere/robossembler-ros2)

## FAQ

* **Зачем печатать мотор, если можно просто купить недорого в Китае?**
    - В самом начале мы рассматривали китайские моторы для позиционирования камер GM6208, но потом отказались от них по ряду причин: они относительно дорогостоящие(а для манипулятора их нужно много), не подходят для автоматической сборки и монтажа(нет мест для позиционирования механического захвата).
* **Какие компоненты не печатаются?**
    - Не печатаются магниты, платы и компоненты, контакты, подшипники, катушки, магнитопровод (М3 болт).
* **Почему Вы заявляете, что open source, а исходники в Solidworks?**
    - Полный перевод исходных файлов проекта будет осуществляться по мере испытаний прототипа и выпуска alpha-версии манипулятора. Чтобы ознакомиться с исходными моделями, можно открыть STEP-файл сборки. Например, с помощью свободных программ [FreeCAD](https://www.freecadweb.org/), [Mayo](https://github.com/fougue/mayo) или [Analysis situs](http://quaoar.su/blog/page/analysis-situs)
* **Почему не применили open source контроллеры моторов - ODrive, MIT Cheetah?**
    - При всём уважении к указанным проектам, платы их контроллеров не подходят к нашей конструкции манипулятора. Также мы старались разработать контроллер из максимально дешёвых и доступных компонентов.

## Участие в проекте

Будем рады принять помощь в любом виде: проектирование, критика, предложения, обнаруженные ошибки. Не стесняйтесь, [создавайте issues](https://gitlab.com/robosphere/roboarm-diy-version/-/issues/new). 

Подписывайтесь на наши медийные ресурсы! Там публикуются результаты проекта и другая информация по теме Open Source промышленной робототехники.
* Youtube-канал [Robosphere](https://www.youtube.com/channel/UC32Xgbsw9XQlN1QH59pe8HA)
* Telegram-канал [@robossembler_ru](https://t.me/robossembler_ru)

## Спонсоры

[![](img/robonomics_logo.png)](https://robonomics.network/)