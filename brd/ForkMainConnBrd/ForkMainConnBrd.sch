EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	3200 3750 3200 3800
Wire Wire Line
	2600 3700 2600 3750
Wire Wire Line
	2600 3750 2700 3750
$Comp
L power:GND #PWR0101
U 1 1 6E5A568F
P 3200 4400
F 0 "#PWR0101" H 3200 4150 50  0001 C CNN
F 1 "GND" H 3205 4227 50  0000 C CNN
F 2 "" H 3200 4400 50  0001 C CNN
F 3 "" H 3200 4400 50  0001 C CNN
	1    3200 4400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 6E5A5B89
P 2700 4400
F 0 "#PWR0102" H 2700 4150 50  0001 C CNN
F 1 "GND" H 2705 4227 50  0000 C CNN
F 2 "" H 2700 4400 50  0001 C CNN
F 3 "" H 2700 4400 50  0001 C CNN
	1    2700 4400
	1    0    0    -1  
$EndComp
$Comp
L power:+24V #PWR0103
U 1 1 6E5A5EB1
P 3200 3100
F 0 "#PWR0103" H 3200 2950 50  0001 C CNN
F 1 "+24V" H 3215 3273 50  0000 C CNN
F 2 "" H 3200 3100 50  0001 C CNN
F 3 "" H 3200 3100 50  0001 C CNN
	1    3200 3100
	1    0    0    -1  
$EndComp
$Comp
L power:+24V #PWR0104
U 1 1 6E5A6291
P 2700 3100
F 0 "#PWR0104" H 2700 2950 50  0001 C CNN
F 1 "+24V" H 2715 3273 50  0000 C CNN
F 2 "" H 2700 3100 50  0001 C CNN
F 3 "" H 2700 3100 50  0001 C CNN
	1    2700 3100
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J1
U 1 1 6380F870
P 2900 3750
F 0 "J1" H 2950 4167 50  0000 C CNN
F 1 "Conn_02x05_Odd_Even" H 2950 4076 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x05_P2.54mm_Vertical" H 2900 3750 50  0001 C CNN
F 3 "~" H 2900 3750 50  0001 C CNN
	1    2900 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	2700 3650 2700 3550
Connection ~ 2700 3550
Wire Wire Line
	2700 3550 2700 3100
Wire Wire Line
	3200 3650 3200 3550
Connection ~ 3200 3550
Wire Wire Line
	2700 3850 2700 3950
Connection ~ 2700 3950
Wire Wire Line
	2700 3950 2700 4400
Wire Wire Line
	3200 3850 3200 3950
Connection ~ 3200 3950
Wire Wire Line
	3200 3950 3200 4400
Text GLabel 2600 3700 0    50   Input ~ 0
canh
Text GLabel 3200 3800 2    50   Input ~ 0
canl
Wire Wire Line
	2600 3750 2150 3750
Connection ~ 2600 3750
Wire Wire Line
	3200 3100 3200 3550
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J2
U 1 1 63910D5C
P 4800 3750
F 0 "J2" H 4850 4167 50  0000 C CNN
F 1 "Conn_02x05_Odd_Even" H 4850 4076 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x05_P2.54mm_Vertical" H 4800 3750 50  0001 C CNN
F 3 "~" H 4800 3750 50  0001 C CNN
	1    4800 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	3200 3800 5250 3800
Wire Wire Line
	5250 3800 5250 3750
Wire Wire Line
	5250 3750 5100 3750
Wire Wire Line
	4600 3750 4600 3700
Wire Wire Line
	4600 3700 2600 3700
Wire Wire Line
	2150 3700 2150 3750
Connection ~ 2600 3700
Wire Wire Line
	2600 3700 2150 3700
Wire Wire Line
	4600 3850 4600 3950
Connection ~ 4600 3950
Wire Wire Line
	4600 3950 4600 4350
Wire Wire Line
	5100 3850 5100 3950
Connection ~ 5100 3950
Wire Wire Line
	5100 3950 5100 4350
Wire Wire Line
	4600 3650 4600 3550
Connection ~ 4600 3550
Wire Wire Line
	4600 3550 4600 3100
Wire Wire Line
	5100 3650 5100 3550
Connection ~ 5100 3550
Wire Wire Line
	5100 3550 5100 3100
$Comp
L power:+24V #PWR0105
U 1 1 63912D7E
P 5100 3100
F 0 "#PWR0105" H 5100 2950 50  0001 C CNN
F 1 "+24V" H 5115 3273 50  0000 C CNN
F 2 "" H 5100 3100 50  0001 C CNN
F 3 "" H 5100 3100 50  0001 C CNN
	1    5100 3100
	1    0    0    -1  
$EndComp
$Comp
L power:+24V #PWR0106
U 1 1 63913304
P 4600 3100
F 0 "#PWR0106" H 4600 2950 50  0001 C CNN
F 1 "+24V" H 4615 3273 50  0000 C CNN
F 2 "" H 4600 3100 50  0001 C CNN
F 3 "" H 4600 3100 50  0001 C CNN
	1    4600 3100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0107
U 1 1 639135DC
P 4600 4350
F 0 "#PWR0107" H 4600 4100 50  0001 C CNN
F 1 "GND" H 4605 4177 50  0000 C CNN
F 2 "" H 4600 4350 50  0001 C CNN
F 3 "" H 4600 4350 50  0001 C CNN
	1    4600 4350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0108
U 1 1 63913993
P 5100 4350
F 0 "#PWR0108" H 5100 4100 50  0001 C CNN
F 1 "GND" H 5105 4177 50  0000 C CNN
F 2 "" H 5100 4350 50  0001 C CNN
F 3 "" H 5100 4350 50  0001 C CNN
	1    5100 4350
	1    0    0    -1  
$EndComp
$EndSCHEMATC
