EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	2350 3850 2350 3900
Wire Wire Line
	1750 3800 1750 3850
Wire Wire Line
	1750 3850 1850 3850
$Comp
L power:GND #PWR0101
U 1 1 6E5A568F
P 2350 4500
F 0 "#PWR0101" H 2350 4250 50  0001 C CNN
F 1 "GND" H 2355 4327 50  0000 C CNN
F 2 "" H 2350 4500 50  0001 C CNN
F 3 "" H 2350 4500 50  0001 C CNN
	1    2350 4500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 6E5A5B89
P 1850 4500
F 0 "#PWR0102" H 1850 4250 50  0001 C CNN
F 1 "GND" H 1855 4327 50  0000 C CNN
F 2 "" H 1850 4500 50  0001 C CNN
F 3 "" H 1850 4500 50  0001 C CNN
	1    1850 4500
	1    0    0    -1  
$EndComp
$Comp
L power:+24V #PWR0103
U 1 1 6E5A5EB1
P 2350 3200
F 0 "#PWR0103" H 2350 3050 50  0001 C CNN
F 1 "+24V" H 2365 3373 50  0000 C CNN
F 2 "" H 2350 3200 50  0001 C CNN
F 3 "" H 2350 3200 50  0001 C CNN
	1    2350 3200
	1    0    0    -1  
$EndComp
$Comp
L power:+24V #PWR0104
U 1 1 6E5A6291
P 1850 3200
F 0 "#PWR0104" H 1850 3050 50  0001 C CNN
F 1 "+24V" H 1865 3373 50  0000 C CNN
F 2 "" H 1850 3200 50  0001 C CNN
F 3 "" H 1850 3200 50  0001 C CNN
	1    1850 3200
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J1
U 1 1 6380F870
P 2050 3850
F 0 "J1" H 2100 4267 50  0000 C CNN
F 1 "Conn_02x05_Odd_Even" H 2100 4176 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x05_P2.54mm_Horizontal" H 2050 3850 50  0001 C CNN
F 3 "~" H 2050 3850 50  0001 C CNN
	1    2050 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	1850 3750 1850 3650
Connection ~ 1850 3650
Wire Wire Line
	1850 3650 1850 3200
Wire Wire Line
	2350 3750 2350 3650
Connection ~ 2350 3650
Wire Wire Line
	1850 3950 1850 4050
Connection ~ 1850 4050
Wire Wire Line
	1850 4050 1850 4500
Wire Wire Line
	2350 3950 2350 4050
Connection ~ 2350 4050
Wire Wire Line
	2350 4050 2350 4500
Wire Wire Line
	2350 3200 2350 3650
Text GLabel 1750 3800 0    50   Input ~ 0
canh
Text GLabel 2350 3900 2    50   Input ~ 0
canl
$EndSCHEMATC
