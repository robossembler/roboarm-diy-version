EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	3200 3750 3200 3800
Wire Wire Line
	2600 3700 2600 3750
Wire Wire Line
	2600 3750 2700 3750
$Comp
L power:GND #PWR0101
U 1 1 6E5A568F
P 3200 4400
F 0 "#PWR0101" H 3200 4150 50  0001 C CNN
F 1 "GND" H 3205 4227 50  0000 C CNN
F 2 "" H 3200 4400 50  0001 C CNN
F 3 "" H 3200 4400 50  0001 C CNN
	1    3200 4400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 6E5A5B89
P 2700 4400
F 0 "#PWR0102" H 2700 4150 50  0001 C CNN
F 1 "GND" H 2705 4227 50  0000 C CNN
F 2 "" H 2700 4400 50  0001 C CNN
F 3 "" H 2700 4400 50  0001 C CNN
	1    2700 4400
	1    0    0    -1  
$EndComp
$Comp
L power:+24V #PWR0103
U 1 1 6E5A5EB1
P 3200 3100
F 0 "#PWR0103" H 3200 2950 50  0001 C CNN
F 1 "+24V" H 3215 3273 50  0000 C CNN
F 2 "" H 3200 3100 50  0001 C CNN
F 3 "" H 3200 3100 50  0001 C CNN
	1    3200 3100
	1    0    0    -1  
$EndComp
$Comp
L power:+24V #PWR0104
U 1 1 6E5A6291
P 2700 3100
F 0 "#PWR0104" H 2700 2950 50  0001 C CNN
F 1 "+24V" H 2715 3273 50  0000 C CNN
F 2 "" H 2700 3100 50  0001 C CNN
F 3 "" H 2700 3100 50  0001 C CNN
	1    2700 3100
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J1
U 1 1 6380F870
P 2900 3750
F 0 "J1" H 2950 4167 50  0000 C CNN
F 1 "Conn_02x05_Odd_Even" H 2950 4076 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x05_P2.54mm_Vertical" H 2900 3750 50  0001 C CNN
F 3 "~" H 2900 3750 50  0001 C CNN
	1    2900 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	2700 3650 2700 3550
Connection ~ 2700 3550
Wire Wire Line
	2700 3550 2700 3100
Wire Wire Line
	3200 3650 3200 3550
Connection ~ 3200 3550
Wire Wire Line
	2700 3850 2700 3950
Connection ~ 2700 3950
Wire Wire Line
	2700 3950 2700 4400
Wire Wire Line
	3200 3850 3200 3950
Connection ~ 3200 3950
Wire Wire Line
	3200 3950 3200 4400
Wire Wire Line
	3200 3100 3200 3400
Text GLabel 2600 3700 0    50   Input ~ 0
canh
Text GLabel 3200 3800 2    50   Input ~ 0
canl
$Comp
L Mechanical:MountingHole_Pad H3
U 1 1 6390E52E
P 3900 3750
F 0 "H3" V 3854 3900 50  0000 L CNN
F 1 "MountingHole_Pad" V 3945 3900 50  0000 L CNN
F 2 "TestPoint:TestPoint_Plated_Hole_D4.0mm" H 3900 3750 50  0001 C CNN
F 3 "~" H 3900 3750 50  0001 C CNN
	1    3900 3750
	0    1    1    0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H2
U 1 1 6390F659
P 3900 3400
F 0 "H2" V 3854 3550 50  0000 L CNN
F 1 "MountingHole_Pad" V 3945 3550 50  0000 L CNN
F 2 "TestPoint:TestPoint_Plated_Hole_D4.0mm" H 3900 3400 50  0001 C CNN
F 3 "~" H 3900 3400 50  0001 C CNN
	1    3900 3400
	0    1    1    0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H4
U 1 1 6390FBAE
P 3900 4400
F 0 "H4" V 3854 4550 50  0000 L CNN
F 1 "MountingHole_Pad" V 3945 4550 50  0000 L CNN
F 2 "TestPoint:TestPoint_Plated_Hole_D4.0mm" H 3900 4400 50  0001 C CNN
F 3 "~" H 3900 4400 50  0001 C CNN
	1    3900 4400
	0    1    1    0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H1
U 1 1 63910375
P 2050 3750
F 0 "H1" V 2287 3753 50  0000 C CNN
F 1 "MountingHole_Pad" V 2196 3753 50  0000 C CNN
F 2 "TestPoint:TestPoint_Plated_Hole_D4.0mm" H 2050 3750 50  0001 C CNN
F 3 "~" H 2050 3750 50  0001 C CNN
	1    2050 3750
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3200 3750 3800 3750
Connection ~ 3200 3750
Wire Wire Line
	2600 3750 2150 3750
Connection ~ 2600 3750
Wire Wire Line
	3200 3400 3800 3400
Connection ~ 3200 3400
Wire Wire Line
	3200 3400 3200 3550
Wire Wire Line
	3200 4400 3800 4400
Connection ~ 3200 4400
$EndSCHEMATC
