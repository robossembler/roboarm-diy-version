### Gitlab CI/CD for KiCad

stages:
  - run_erc
  - run_drc
  - gen_fab
  - upload_package
  - release

variables:
  ARCHIVE_NAME: '${PACKAGE_NAME}-${CI_COMMIT_SHORT_SHA}'
  PACKAGE_REGISTRY_URL: "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${PACKAGE_NAME}/${CI_COMMIT_SHORT_SHA}"
  KIBOT_CONF: '${CI_PROJECT_DIR}/kicad_ci_test.kiplot.yaml'
  PCB_OPTIONS: 'print_front print_bottom gerbers gerber_drills pcb_top_g pcb_bot_g pcb_top_b pcb_bot_b pcb_top_r pcb_bot_r step position PCBWay_drill PCBWay_gerbers PCBWay' 
  SCH_OPTIONS: 'print_sch bom_html interactive_bom bom_xlsx bom_csv'

image:
  name: setsoft/kicad_auto:dev_k6
  pull_policy: if-not-present

# Default properties

default:
  before_script:
    - cd ${CI_PROJECT_DIR}/${FOLDER}

.sch-rules:
  allow_failure: true
  rules:
    - if: $SCHEMATICS == "false"
      when: never
    - changes:
      - "${FOLDER}/**.kicad_sch"
      - "${FOLDER}/**.sch"
      - ".gitlab-ci.yml"
      - ".kicad-pipeline.yml"
      when: on_success

.pcb-rules:
  allow_failure: true
  rules:
    - changes:
      - "${FOLDER}/**.kicad_pcb"
      - ".gitlab-ci.yml"
      - ".kicad-pipeline.yml"
      when: on_success

.default-artifacts:
  artifacts:
      when: always
      paths:
        - Fabrication
      untracked: true

# Jobs

erc:
  stage: run_erc
  script:
    - kibot -d Fabrication -c ${KIBOT_CONF} -s update_xml,run_drc -i
  extends:
    - .default-artifacts
    - .sch-rules

drc:
  stage: run_drc
  script:
    - kibot -d Fabrication -c ${KIBOT_CONF} -s update_xml,run_erc -i
  extends:
    - .pcb-rules
    - .default-artifacts

sch_outputs:
  stage: gen_fab
  script:
    - kibot -d Fabrication -c ${KIBOT_CONF} -s run_drc,run_erc ${SCH_OPTIONS}
  extends: 
    - .sch-rules
    - .default-artifacts

pcb_outputs:
  stage: gen_fab
  script:
    - kibot -d Fabrication -c ${KIBOT_CONF} -s all ${PCB_OPTIONS}
  extends:
    - .pcb-rules
    - .default-artifacts

upload_package:
  stage: upload_package
  image: bash:latest
  rules:
        - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
  extends:
    - .pcb-rules
    - .default-artifacts
  script:
    - pwd
    - ls -la ${CI_PROJECT_DIR}
    - ls -la
    - tar -czvf "${ARCHIVE_NAME}.tar.gz" Fabrication
    - |
      apk add curl
      echo "${PACKAGE_REGISTRY_URL}/${ARCHIVE_NAME}-${CI_COMMIT_TAG}.tar.gz"
      curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file "${ARCHIVE_NAME}.tar.gz" "${PACKAGE_REGISTRY_URL}/${ARCHIVE_NAME}.tar.gz"

release:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - |
      release-cli create --name "Release $PACKAGE_NAME" --tag-name "${CI_COMMIT_SHORT_SHA}" \
        --assets-link "{\"name\":\"${PACKAGE_NAME}\",\"url\":\"${PACKAGE_REGISTRY_URL}/${ARCHIVE_NAME}.tar.gz\"}"

